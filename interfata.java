package exam;

import java.awt.TextArea;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

class interfata extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	JTextField tf1,tf2,tf3;
	TextArea ta1;
	interfata(String s)
	{
		setLayout(null);
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(500,500);
		metoda();
	}
	public void metoda()
	{
		tf1 = new JTextField(10);
		tf1.setBounds(40,20,50,20);
		tf2 = new JTextField(10);
		tf2.setBounds(40,50,100,20);
		ta1= new TextArea();
		ta1.setBounds(40, 70, 100,50);
		ta1.setEditable(false);
		JButton b1= new JButton("APASA");
		b1.setBounds(40,150,100,20);
		add(tf1);
		add(tf2);
		add(ta1);
		add(b1);
		b1.addMouseListener(new MouseListener() {


			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				int value = Integer.parseInt(tf1.getText()) + Integer.parseInt(tf2.getText());
				ta1.setText("\n"+ value);
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
	}
	
	public static void main(String...strings)
	{
		interfata ob = new interfata("Examen");
		ob.setSize(500,500);
		ob.setVisible(true);
	}
}


